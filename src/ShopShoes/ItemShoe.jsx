import React from "react";

export default function ItemShoe({ shoe, addToCart }) {
  return (
    <div className="col-3 mt-3">
      <div className="card" style={{ width: "100" }}>
        <img className="card-img-top" src={shoe.image} alt="Card image cap" />
        <div className="card-body">
          <h6 className="card-title">{shoe.name}</h6>
          <p className="card-text">{shoe.shortDescription}</p>
          <button
            onClick={() => {
              addToCart(shoe);
            }}
            type="button"
            className="btn btn-primary"
          >
            <span className="mr-2">Add to Cart</span>
            <i className="fa fa-shopping-cart"></i>
          </button>
        </div>
      </div>
    </div>
  );
}
