import React, { useState } from "react";
import Cart from "./Cart";
import { data_shoes } from "./data_shoes";
import ItemShoe from "./ItemShoe";

export default function ShopShoes() {
  let [shoes, setShoes] = useState(data_shoes);
  let [cart, setCart] = useState([]);

  let renderContent = () => {
    return shoes.map((shoe, index) => {
      return <ItemShoe key={index} shoe={shoe} addToCart={addToCart} />;
    });
  };

  let addToCart = (shoe) => {
    let index = cart.findIndex((item) => {
      return item.id === shoe.id;
    });
    let cloneCart = [...cart];
    if (index === -1) {
      let newProduct = { ...shoe, quantity: 1 };
      cloneCart.push(newProduct);
    } else {
      cloneCart[index].quantity++;
    }
    setCart(cloneCart);
  };

  let handleChangeQuantity = (idShoe, value) => {
    let index = cart.findIndex((item) => {
      return item.id === idShoe;
    });
    if (index === -1) {
      return;
    }
    let cloneCart = [...cart];
    cloneCart[index].quantity += value;
    if (cloneCart[index].quantity === 0) {
      cloneCart.splice(index, 1);
    }
    setCart(cloneCart);
  };
  return (
    <div className="container py-5">
      <Cart cart={cart} handleChangeQuantity={handleChangeQuantity} />
      <div className="row">{renderContent()}</div>
    </div>
  );
}
